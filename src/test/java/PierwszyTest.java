import org.junit.jupiter.api.*;

public class PierwszyTest {
    @BeforeAll
    public static void start() {  //powinna być static, żeby była uruchomiana przed wszystkim
        System.out.println("przed wszystkimi testami");
    }
    @AfterAll
    public static void end() {  //powinna być static??
        System.out.println("po wszystkich testach");
    }

    Samochod auto; //deklaracja obiektu klasy samochów, żeby był widoczny dla wszystkich
                    // to wyrzucamy przed wszystko jesli mamy dużo testów które startują z tego samego to możemy soboie uprścić

    @BeforeEach  //ten blok metody będzie uruchamiany przed każdym testem, mamy pewność że nic się nie zmieni
    public void before() {
        auto = new Samochod(100, "Toyota");
    }

    @AfterEach      //wykonywana po każdym teście
    public void after() {
        System.out.println("Tekst po tescie");
        //służy do niszczenia plików i obiektów
    }

    @Test
    public void testMnozenia() {
        //Given
        int a = 3;
        int b = 6;

        //When
        int wynik = a * b;

        //Than
        Assertions.assertEquals(18, wynik);
    }

    @Test
    public void testDodawania() {
        //Given
        int a = 3;
        int b = 6;

        //When
        int wynik = a + b;

        //Then
        Assertions.assertEquals(9, wynik);
    }

    @Test
    public void testLiter() {
        //Given
        char litera = 'a';
        boolean czyLiteraA = false;

        //When
        if (litera == 'a') {
            czyLiteraA = true;
        }
        //Then
        Assertions.assertEquals(true, czyLiteraA);
        Assertions.assertTrue(czyLiteraA);
        Assertions.assertFalse(!czyLiteraA);
    }

    @Test
    public void zmiennaWypełniona() {
        //Given
        String imie = null;

        //When
        if (imie == null) {
            imie = "Paweł";
        }

        //Then
        Assertions.assertNotNull(imie);
       // Assertions.assertNull(imie);
    }

    @Test
    public void testSamochodPredkosc() {
        //Given
        int spodziewanaPredkosc = 100;
        /* To już nie potrzebne bo daliśmy do Beforeeach
        Samochod auto = new Samochod(spodziewanaPredkosc, "Toyota");
         */

        //When
        int realnaPredkosc = auto.getPredkosc();

        //Then
        Assertions.assertEquals(spodziewanaPredkosc,realnaPredkosc);
    }

    @Test
    public void testSamochodMarka() {
        //Given
        String spodziewanaMarka = "Toyota";

        /* To już nie potrzebne bo daliśmy do Beforeeach
        Samochod auto = new Samochod(spodziewanaPredkosc, "Toyota");
        */

        //When
        String realnaMarka = auto.getMarka();

        //Then
        Assertions.assertEquals(spodziewanaMarka,realnaMarka);
    }

}
