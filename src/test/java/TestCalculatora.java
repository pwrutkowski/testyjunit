import calculator.Calculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestCalculatora {

    Calculator calc2El;
    Calculator calc3El;

    @BeforeEach
    public void before() {
        int[] dwaEl = {10,5};
        int[] trzyEl = {20, 5, 2};
        calc2El = new Calculator(dwaEl);
        calc3El = new Calculator(trzyEl);
    }

    @Test
    public void testDodawaniaDwoch() {
        //Given

        //When
        int wynik = calc2El.getSum();
        //Then
        Assertions.assertEquals(15, wynik);
    }

    @Test
    public void testDodawaniaDwochSkrajnych() {
        //Given
        int[] skrajneInty = {-2147483648,2147483647};
        calc2El.setArray(skrajneInty);
        //When
        int wynik = calc2El.getSum();
        //Then
        Assertions.assertEquals(-1, wynik);
    }

    @Test
    public void testDodawaniaTrzech() {
        //Given

        //When
        int wynik = calc3El.getSum();

        //Then
        Assertions.assertEquals(27, wynik);
    }

    @Test
    public void testOdejmowaniaDwoch() {
        //Given

        //When
        int wynik = calc2El.getDiff();
        //Then
        Assertions.assertEquals(5, wynik);
    }

    @Test
    public void testOdejmowaniaDwochSkrajnychUjemnych() {
        //Given
        int[] skrajneInty = {-2147483648,-2147483647};
        calc2El.setArray(skrajneInty);
        //When
        int wynik = calc2El.getDiff();
        //Then
        Assertions.assertEquals(-1, wynik);
    }
    @Test
    public void testOdejmowaniaDwochSkrajnychDodatnich() {
        //Given
        int[] skrajneInty = {2147483647,2147483646};
        calc2El.setArray(skrajneInty);
        //When
        int wynik = calc2El.getDiff();
        //Then
        Assertions.assertEquals(1, wynik);
    }

    @Test
    public void testOdejmowaniaTrzech() {
        //Given

        //When
        int wynik = calc3El.getDiff();
        //Then
        Assertions.assertEquals(13, wynik);
    }

    @Test
    public void testMnozeniaDwoch() {
        //Given

        //When
        int wynik = calc2El.getMult();
        //Then
        Assertions.assertEquals(50, wynik);
    }

    @Test
    public void testMnozeniaTrzech() {
        //Given

        //When
        int wynik = calc3El.getMult();
        //Then
        Assertions.assertEquals(200, wynik);
    }

    @Test
    public void testMnozeniaZero() {
        //Given
        int[] dwaElZero1 = {0,5};
        calc2El.setArray(dwaElZero1);

        //When
        int wynik = calc2El.getMult();

        //Then
        Assertions.assertEquals(0, wynik);
    }

    @Test
    public void testDzieleniaDwoch() {
        //Given

        //When
        double wynik = calc2El.getDiv();

        //Then
        Assertions.assertEquals(2, wynik);
    }

    @Test
    public void testDzieleniaTrzech() {
        //Given

        //When
        double wynik = calc3El.getDiv();

        //Then
        Assertions.assertEquals(2, wynik);
    }

    @Test
    public void testDzieleniaZera() {
        //Given
        int[] dwaElZero1 = {0,5};
        calc2El.setArray(dwaElZero1);

        //When
        double wynik = calc2El.getDiv();

        //Then
        Assertions.assertEquals(0, wynik);
    }
    @Test
    public void testDzieleniaPrzezZero() {

            //Given
            int[] dwaElZero2 = {5, 0};
            calc2El.setArray(dwaElZero2);

            //When
                        //Tu zaczynamy blok który spodziewa się wywalenia wyjątku z jakiegoś działania
        Exception except = Assertions.assertThrows(ArithmeticException.class, () -> {  //definicja jakiego błędu się spodziewamy (arytmetycznego)
            calc2El.getDiv();  //to jest to działanie
        });         // to jest klamra i nawias końca tego bloku
        String actualMessage = except.getMessage();
        String expectedMessage = "by zero";     //spodziewamy się że wywali błąd dzielenia przez zero

            //Then
        Assertions.assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void testModDwoch() {
        //Given

        //When
        int wynik = calc2El.getMod();

        //Then
        Assertions.assertEquals(0, wynik);
    }

}
