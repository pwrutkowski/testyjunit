package calculator;

import java.util.Scanner;

public class CalculatorMain {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.println("Podaj ile liczb chcesz wprowadzić");
        int ileLiczb = Integer.parseInt(scan.nextLine());

        int[] array = new int[ileLiczb];

        for (int kol = 0; kol < array.length; kol++) {
            System.out.println("Podaj liczbę nr " + kol + ": ");
            int n = Integer.parseInt(scan.nextLine());
            array[kol] = n;
        }


        Calculator calculator = new Calculator(array);
        System.out.println("Suma: " + calculator.getSum());
        System.out.println("Różnica: " + calculator.getDiff());
        System.out.println("Mnożenie: " + calculator.getMult());
        System.out.println("Dzielenie: " + calculator.getDiv());
        System.out.println("Modulo: " + calculator.getMod());

        System.out.println("Jaką operację chcesz wykonać");
        String operacja = scan.nextLine();
        switch (operacja) {
            case "dodawanie":
                System.out.println("Suma: " + calculator.getSum());
                break;
            case "odejmowanie":
                System.out.println("Różnica: " + calculator.getDiff());
                break;
            case "mnozenie":
                System.out.println("Mnożenie: " + calculator.getMult());
                break;
            case "dzielenie":
                System.out.println("Dzielenie: " + calculator.getDiv());
                break;
            case "modulo":
                System.out.println("Modulo: " + calculator.getMod());
                break;
            default:
                System.out.println("Wpis nierozpoznany");
                break;

        }



    }
}
