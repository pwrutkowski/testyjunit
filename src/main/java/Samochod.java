public class Samochod {
    private  int predkosc;
    private String marka;

    public Samochod(int predkosc, String marka) {
        this.predkosc = predkosc;
        this.marka = marka;
    }
    public void wyswietlPredkosc(){
        System.out.println(marka+ " "+predkosc);
    }
    public int getPredkosc() {
        return predkosc;
    }
    public String getMarka() {
        return marka;
    }
}
